# JS Runtime
FROM node:19-alpine3.16 as api

WORKDIR /api
COPY api .
RUN npm install

EXPOSE 8888

CMD ["npm", "run", "start:prod"]

FROM node:19-alpine3.16 as client

WORKDIR /client
COPY client .
RUN npm install
RUN npm run build

FROM nginx:mainline-alpine3.18-slim
# Remove the default Nginx configuration file
RUN rm /etc/nginx/conf.d/default.conf

# Copy the static files from the Vite build stage to the Nginx container
COPY --from=client /client/dist /usr/share/nginx/html

# Expose the port for Nginx (adjust as needed)
EXPOSE 80

# Start Nginx when the container starts
CMD ["nginx", "-g", "daemon off;"]